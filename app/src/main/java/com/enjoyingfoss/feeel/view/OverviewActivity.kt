/*
 * This file is part of Feeel.
 *
 *     Feeel is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Feeel is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Feeel.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.enjoyingfoss.feeel_legacy.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.enjoyingfoss.feeel_legacy.R
import kotlinx.android.synthetic.main.activity_overview.*
import java.lang.ref.WeakReference
import com.enjoyingfoss.feeel_legacy.model.WorkoutListItem
import com.enjoyingfoss.feeel_legacy.model.WorkoutDBHelper


class OverviewActivity : AppCompatActivity() {
    //todo make sure the exercise view loads back if service is in background

    var dbHelper: WorkoutDBHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) { //todo connect service and preload here
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_overview)

        dbHelper = WorkoutDBHelper(this)

        workoutRV.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        workoutRV.adapter = WorkoutAdapter(
                dbHelper?.listDBWorkouts(),
                //WorkoutRepository.retrieveAll(),
                WeakReference(workoutRV))
        workoutRV.addOnItemTouchListener(object : androidx.recyclerview.widget.RecyclerView.SimpleOnItemTouchListener() {

        })
    }

    internal class WorkoutAdapter(private val workoutListItems: List<WorkoutListItem>?, private val recyclerView: WeakReference<androidx.recyclerview.widget.RecyclerView>)
        : androidx.recyclerview.widget.RecyclerView.Adapter<WorkoutAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.item_workout, parent, false)
            view.setOnClickListener {
                val recycler = recyclerView.get()

                val itemPosition = recycler?.getChildAdapterPosition(view) ?: 0
                val workout = workoutListItems?.get(itemPosition)

                workout?.apply {
                    val startIntent = Intent(recycler?.context, CoverActivity::class.java)
                    startIntent.putExtra(CoverActivity.WORKOUT_KEY, this)
                    recycler?.context?.startActivity(startIntent)
                }
            }
            return ViewHolder(view)
        }

        override fun getItemCount() = workoutListItems?.size ?: 0

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            workoutListItems?.get(position)?.titleResource?.apply {
                holder.title.setText(this)
            }
        }

        internal class ViewHolder(item: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(item) {
            var title = item.findViewById<View>(R.id.title) as TextView
        }
    }
}