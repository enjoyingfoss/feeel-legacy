/*
 * This file is part of Feeel.
 *
 *     Feeel is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Feeel is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Feeel.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.enjoyingfoss.feeel_legacy.model

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.util.ArrayList

class WorkoutDBHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) { // TODO CLOSE !!!!!
    companion object {
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "workout.db"
    }

    private object DBContract {
        object Workouts {
            const val TABLE_NAME = "workouts"
            const val COLUMN_ID = "id"
            const val COLUMN_TITLE_RES = "title_res"
            const val COLUMN_TITLE = "title" // fallback if name_res is null
            const val COLUMN_COLOR = "color"
            const val COLUMN_BREAK_LENGTH = "break_length"
        }

        const val SQL_CREATE_WORKOUTS =
                "CREATE TABLE ${Workouts.TABLE_NAME} (" +
                        "${Workouts.COLUMN_ID} INTEGER PRIMARY KEY," +
                        "${Workouts.COLUMN_TITLE_RES} INTEGER," +
                        "${Workouts.COLUMN_TITLE} TEXT," +
                        "${Workouts.COLUMN_COLOR} INTEGER," + //todo is this long enough for long?
                        "${Workouts.COLUMN_BREAK_LENGTH} INTEGER)"
        const val SQL_DELETE_WORKOUTS = "DROP TABLE IF EXISTS ${Workouts.TABLE_NAME}"

        object ExerciseMetas {
            const val TABLE_NAME = "exercise_metas"
            const val COLUMN_WORKOUT_ID = "workout_id"
            const val COLUMN_POSITION = "position"
            const val COLUMN_EXERCISE_ID = "exercise_id" // fallback if name_res is null
            const val COLUMN_DURATION = "duration"
            const val COLUMN_FLIPPED_IMAGE = "flipped_image"
        }

        const val SQL_CREATE_EXERCISE_METAS =
                "CREATE TABLE ${ExerciseMetas.TABLE_NAME} (" +
                        "${ExerciseMetas.COLUMN_WORKOUT_ID} INTEGER," +
                        "${ExerciseMetas.COLUMN_POSITION} INTEGER," +
                        "${ExerciseMetas.COLUMN_EXERCISE_ID} INTEGER," +
                        "${ExerciseMetas.COLUMN_DURATION} INTEGER," +
                        "${ExerciseMetas.COLUMN_FLIPPED_IMAGE} INTEGER," +
                        "PRIMARY KEY (${ExerciseMetas.COLUMN_WORKOUT_ID}, ${ExerciseMetas.COLUMN_POSITION}))"
        const val SQL_DELETE_EXERCISE_METAS = "DROP TABLE IF EXISTS ${ExerciseMetas.TABLE_NAME}"

        object Exercises {
            const val TABLE_NAME = "exercises"
            const val COLUMN_ID = "id"
            const val COLUMN_TITLE_RES = "title_res"
            const val COLUMN_DESC_RES = "desc_res"
            const val COLUMN_IMAGE_RES = "image_res"
        }

        const val SQL_CREATE_EXERCISES =
                "CREATE TABLE ${Exercises.TABLE_NAME} (" +
                        "${Exercises.COLUMN_ID} INTEGER PRIMARY KEY," +
                        "${Exercises.COLUMN_TITLE_RES} INTEGER," +
                        "${Exercises.COLUMN_DESC_RES} INTEGER," +
                        "${Exercises.COLUMN_IMAGE_RES} INTEGER)"
        const val SQL_DELETE_EXERCISES = "DROP TABLE IF EXISTS ${Workouts.TABLE_NAME}"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(DBContract.SQL_CREATE_WORKOUTS)
        db.execSQL(DBContract.SQL_CREATE_EXERCISE_METAS)
        db.execSQL(DBContract.SQL_CREATE_EXERCISES)

        insertDefaults(db)
    }

    private fun insertDefaults(db: SQLiteDatabase) { //todo does this ensure a unique, auto-incrementing id???
        db.beginTransaction()
        try {
            for (workout in WorkoutRepository.retrieveAll()) {
                val workoutValues = ContentValues().apply {
                    put(DBContract.Workouts.COLUMN_TITLE_RES, workout.titleResource)
                    put(DBContract.Workouts.COLUMN_COLOR, workout.customColor)
                    put(DBContract.Workouts.COLUMN_BREAK_LENGTH, workout.breakLength)
                }
                val workoutId = db.insert(DBContract.Workouts.TABLE_NAME, null, workoutValues)

                var position = 1
                for (exerciseMeta in workout.exerciseMetas) {
                    val exercise = exerciseMeta.exercise
                    val exerciseValues = ContentValues().apply {
                        put(DBContract.Exercises.COLUMN_TITLE_RES, exercise.titleResource)
                        put(DBContract.Exercises.COLUMN_DESC_RES, exercise.descResource)
                        put(DBContract.Exercises.COLUMN_IMAGE_RES, exercise.imageResource)
                    }
                    val exerciseId = db.insert(DBContract.Exercises.TABLE_NAME, null, exerciseValues)
                    val exerciseMetaValues = ContentValues().apply {
                        put(DBContract.ExerciseMetas.COLUMN_WORKOUT_ID, workoutId)
                        put(DBContract.ExerciseMetas.COLUMN_POSITION, position)
                        put(DBContract.ExerciseMetas.COLUMN_DURATION, exerciseMeta.duration)
                        put(DBContract.ExerciseMetas.COLUMN_EXERCISE_ID, exerciseId)
                        put(DBContract.ExerciseMetas.COLUMN_FLIPPED_IMAGE, if (exerciseMeta.isFlipped) 1 else 0)
                    }
                    db.insert(DBContract.ExerciseMetas.TABLE_NAME, null, exerciseMetaValues)
                    position++
                }
            }
            db.setTransactionSuccessful()
        } finally {
            db.endTransaction()
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {}

    fun listDBWorkouts(): List<WorkoutListItem> { //tbd different thread
        val cursor = readableDatabase.rawQuery(
                "SELECT * FROM ${DBContract.Workouts.TABLE_NAME}", null
        )
        val workoutList = ArrayList<WorkoutListItem>()
        while (cursor.moveToNext()) {
            workoutList.add(WorkoutListItem(
                    id = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.Workouts.COLUMN_ID)),
                    titleResource = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.Workouts.COLUMN_TITLE_RES)),
                    title = cursor.getString(cursor.getColumnIndexOrThrow(DBContract.Workouts.COLUMN_TITLE)),
                    color = cursor.getLong(cursor.getColumnIndexOrThrow(DBContract.Workouts.COLUMN_COLOR)),
                    breakLength = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.Workouts.COLUMN_BREAK_LENGTH))
            ))
        }
        return workoutList
    }

    fun listExerciseMetas(workoutId: Int): Array<ExerciseMeta> { //tbd different thread
        val cursorMeta = readableDatabase.rawQuery(
                "SELECT * FROM ${DBContract.ExerciseMetas.TABLE_NAME} WHERE ${DBContract.ExerciseMetas.COLUMN_WORKOUT_ID} = $workoutId",
                null
        )
        val dbMetaArray = ArrayList<DBExerciseMeta>()
        while (cursorMeta.moveToNext()) {
            dbMetaArray.add(DBExerciseMeta(
                    workoutId = cursorMeta.getInt(cursorMeta.getColumnIndexOrThrow(DBContract.ExerciseMetas.COLUMN_WORKOUT_ID)),
                            position = cursorMeta.getInt(cursorMeta.getColumnIndexOrThrow(DBContract.ExerciseMetas.COLUMN_POSITION)),
                    exerciseId = cursorMeta.getInt(cursorMeta.getColumnIndexOrThrow(DBContract.ExerciseMetas.COLUMN_EXERCISE_ID)),
                    duration = cursorMeta.getInt(cursorMeta.getColumnIndexOrThrow(DBContract.ExerciseMetas.COLUMN_DURATION)),
                    flippedImage = cursorMeta.getInt(cursorMeta.getColumnIndexOrThrow(DBContract.ExerciseMetas.COLUMN_FLIPPED_IMAGE)) == 1
                    )
            )
        }

        return Array<ExerciseMeta>(dbMetaArray.size) {
            val dbMeta = dbMetaArray[it]
            val cursorExercise = readableDatabase.rawQuery(
                    "SELECT * FROM ${DBContract.Exercises.TABLE_NAME} WHERE ${DBContract.Exercises.COLUMN_ID} = ${dbMeta.exerciseId}",
                    null
            )
            cursorExercise.moveToFirst()
            val exercise = Exercise(
                    imageResource = cursorExercise.getInt(cursorExercise.getColumnIndexOrThrow(DBContract.Exercises.COLUMN_IMAGE_RES)),
                    titleResource = cursorExercise.getInt(cursorExercise.getColumnIndexOrThrow(DBContract.Exercises.COLUMN_TITLE_RES)),
                    descResource = cursorExercise.getInt(cursorExercise.getColumnIndexOrThrow(DBContract.Exercises.COLUMN_DESC_RES))
            )
            (ExerciseMeta(
                    exercise = exercise,
                    duration = dbMeta.duration,
                    isFlipped = dbMeta.flippedImage
            ))
        }
    }
}